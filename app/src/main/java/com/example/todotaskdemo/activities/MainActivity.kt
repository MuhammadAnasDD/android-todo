package com.example.todotaskdemo.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.todotaskdemo.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {
        add_task.setOnClickListener {
            startActivity(Intent(this,Task::class.java))
        }

        view_task.setOnClickListener{
            startActivity(Intent(this,TaskList::class.java))
        }
    }
}
