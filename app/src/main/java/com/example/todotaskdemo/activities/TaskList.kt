package com.example.todotaskdemo.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.example.todotaskdemo.R
import com.example.todotaskdemo.TaskModel
import com.example.todotaskdemo.adapters.TaskCustomAdapter
import com.example.todotaskdemo.helper.Constants
import com.example.todotaskdemo.utility.SharedPrefManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_task_list.*

class TaskList : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task_list)

        init()
    }

    private fun init() {
        val arrayList: ArrayList<TaskModel>
        if (SharedPrefManager(this).getStringByKey(Constants.ALL_TASK) != null){
            val storedList = SharedPrefManager(this).getStringByKey(Constants.ALL_TASK)
            val gson = Gson()
            val type = object : TypeToken<List<TaskModel>>() {}.type
            arrayList =  gson.fromJson(storedList, type)
            val taskCustomAdapter = TaskCustomAdapter(arrayList)
            val mLayoutManager = LinearLayoutManager(this)
            recyclerView.layoutManager = mLayoutManager
            recyclerView.adapter = taskCustomAdapter
            taskCustomAdapter.setOnItemClickListener(object : TaskCustomAdapter.ItemClickListener {
                override fun onItemClick(position: Int, view: View) {
                    when(view.id){
                        R.id.editTask -> {
                            val intent = Intent(this@TaskList,Task::class.java)
                            intent.putExtra(Constants.TASK_OBJECT,arrayList[position])
                            intent.putExtra(Constants.TASK_UPDATE,position)
                            startActivity(intent)
                        }
                        R.id.removeTask -> {
                            taskCustomAdapter.removeItem(position)
                            SharedPrefManager(this@TaskList).setValue(Constants.ALL_TASK,gson.toJson(arrayList))
                            Toast.makeText(this@TaskList,"Task Removed", Toast.LENGTH_LONG).show()
                        }
                    }
                }
            })
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
