package com.example.todotaskdemo.activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.TimePickerDialog
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.CalendarContract
import android.support.v4.app.ActivityCompat
import android.util.Log
import android.widget.Toast
import com.example.todotaskdemo.R
import com.example.todotaskdemo.TaskModel
import com.example.todotaskdemo.helper.Constants
import com.example.todotaskdemo.utility.SharedPrefManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder
import kotlinx.android.synthetic.main.activity_task.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class Task : AppCompatActivity() {

    private var myCalendar: Calendar? = null
    private var date : String = ""
    private var arrayList = ArrayList<TaskModel>()
    private val gson = Gson()

    companion object {
        const val MY_PERMISSIONS_REQUEST_ACCESS_WRITE_CALENDER = 1001
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task)

        init()
    }

    private var extras: Bundle? = null

    private fun init() {

        myCalendar = Calendar.getInstance()

        extras = intent.extras
        if (extras != null){
            val taskModel = intent.getSerializableExtra(Constants.TASK_OBJECT) as TaskModel
            taskName.setText(taskModel.taskName)
            taskDescription.setText(taskModel.taskDiscription)
            checkBox.isChecked = taskModel.isSetReminder
            reminderTV.text = taskModel.reminderDateTime
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_CALENDAR), MY_PERMISSIONS_REQUEST_ACCESS_WRITE_CALENDER)

        }

        checkBox.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                SpinnerDatePickerDialogBuilder()
                        .context(this)
                        .callback { view1, year, monthOfYear, dayOfMonth ->
                            myCalendar!!.set(year, monthOfYear, dayOfMonth)
                            val sdf = SimpleDateFormat(Constants.DATE_FORMATE, Locale.US)
                            date = sdf.format(myCalendar!!.time)
                            timePickerDailog()
                        }.minDate(myCalendar!!.get(Calendar.YEAR), myCalendar!!.get(Calendar.MONTH),
                                myCalendar!!.get(Calendar.DAY_OF_MONTH))
                        .defaultDate(myCalendar!!.get(Calendar.YEAR), myCalendar!!.get(Calendar.MONTH),
                                myCalendar!!.get(Calendar.DAY_OF_MONTH))
                        .build()
                        .show()
            }
        }

        saveTaskBtn.setOnClickListener {
            if (taskName.text.isNotEmpty()){
                 val taskModel = TaskModel()
                taskModel.taskName = taskName.text.toString()
                taskModel.taskDiscription = taskDescription.text.toString()
                taskModel.isSetReminder = checkBox.isChecked
                if (checkBox.isChecked && reminderTV.text.toString() != "Reminder") {
                    taskModel.reminderDateTime = reminderTV.text.toString()
                    addToDeviceCalendar(reminderTV.text.toString(),date,taskName.text.toString(),taskDescription.text.toString(),"")
                }
                if (SharedPrefManager(this).getStringByKey(Constants.ALL_TASK) != null){
                    val storedList = SharedPrefManager(this).getStringByKey(Constants.ALL_TASK)
                    val type = object : TypeToken<List<TaskModel>>() {}.type
                    arrayList =  gson.fromJson(storedList, type)
                    if (extras == null) {
                        arrayList.add(taskModel)
                        Toast.makeText(this, "Task Added", Toast.LENGTH_LONG).show()
                    } else {
                        arrayList[intent.extras.getInt(Constants.TASK_UPDATE,0)] = taskModel
                        Toast.makeText(this,"Task Updated",Toast.LENGTH_LONG).show()
                    }
                }else {
                    arrayList.add(taskModel)
                    Toast.makeText(this,"Task Added",Toast.LENGTH_LONG).show()
                }
                SharedPrefManager(this).setValue(Constants.ALL_TASK,gson.toJson(arrayList))
                startActivity(Intent(this,TaskList::class.java).also {
                    it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                })
                finish()
            }else {
                Toast.makeText(this,"Please enter task name",Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun timePickerDailog(){
        val mcurrentTime = Calendar.getInstance()
        val sdf = SimpleDateFormat(Constants.TIME_FORMAT)
        val mTimePicker: TimePickerDialog
        mTimePicker = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener { timePicker, selectedHour, selectedMinute ->
            mcurrentTime.set(Calendar.HOUR_OF_DAY, selectedHour)
            mcurrentTime.set(Calendar.MINUTE, selectedMinute)
            val time = sdf.format(mcurrentTime.time)
            reminderTV.text = "$date $time"
        }, mcurrentTime.get(Calendar.HOUR_OF_DAY), mcurrentTime.get(Calendar.MINUTE), true)//Yes 24 hour time
        mTimePicker.setTitle("Select Time")
        mTimePicker.show()
    }

    private fun addToDeviceCalendar(startDate: String, endDate: String, title: String, description: String, location: String) {

        var stDate = startDate
        val enDate = endDate

        val calDate = GregorianCalendar()
        //GregorianCalendar calEndDate = new GregorianCalendar();

        val originalFormat = SimpleDateFormat(Constants.DATE_FORMATE + " " + Constants.TIME_FORMAT)
        val targetFormat = SimpleDateFormat("yyyy,MM,dd,HH,mm")
        val date: Date
        val edate: Date
        try {
            date = originalFormat.parse(startDate)
            stDate = targetFormat.format(date)

        } catch (ex: ParseException) {
            ex.printStackTrace()
        }

        var startMillis: Long = 0
        val endMillis: Long = 0
        //val dates = stDate.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val dates = stDate.split(",".toRegex())

        val SD_YeaR = dates[0]
        val SD_MontH = dates[1]
        val SD_DaY = dates[2]
        val SD_HouR = dates[3]
        val SD_MinutE = dates[4]


        /*Log.e("YeaR ", SD_YeaR);
        Log.e("MontH ",SD_MontH );
        Log.e("DaY ", SD_DaY);
        Log.e(" HouR", SD_HouR);
        Log.e("MinutE ", SD_MinutE);*/

        calDate.set(Integer.parseInt(SD_YeaR), Integer.parseInt(SD_MontH) - 1, Integer.parseInt(SD_DaY), Integer.parseInt(SD_HouR), Integer.parseInt(SD_MinutE))
        startMillis = calDate.timeInMillis

        val hasAlarm = 1
        try {
            val cr = this.contentResolver
            val values = ContentValues()
            values.put(CalendarContract.Events.DTSTART, startMillis)
            values.put(CalendarContract.Events.DTEND, calDate.timeInMillis + 60 * 60 * 1000)
            values.put(CalendarContract.Events.TITLE, title)
            values.put(CalendarContract.Events.DESCRIPTION, description)
            //values.put(CalendarContract.Events.EVENT_LOCATION, location)
            values.put(CalendarContract.Events.HAS_ALARM, true)

            values.put(CalendarContract.Events.CALENDAR_ID, 1)
            values.put(CalendarContract.Events.EVENT_TIMEZONE, Calendar.getInstance()
                    .timeZone.id)
            println(Calendar.getInstance().timeZone.id)
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {

                return
            }
            @SuppressLint("MissingPermission")
            val uri = cr.insert(CalendarContract.Events.CONTENT_URI, values)

            val eventId = java.lang.Long.parseLong(uri!!.lastPathSegment)
            Log.d("Ketan_Event_Id", eventId.toString())
            if (hasAlarm == 1) {
                val reminders = ContentValues()
                reminders.put(CalendarContract.Reminders.EVENT_ID, eventId)
                reminders.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT)
                reminders.put(CalendarContract.Reminders.MINUTES, 60)

                val uri2 = cr.insert(CalendarContract.Reminders.CONTENT_URI, reminders)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

}
