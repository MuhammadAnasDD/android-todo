package com.example.todotaskdemo.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.example.todotaskdemo.R
import com.example.todotaskdemo.TaskModel

class TaskCustomAdapter(private var list: ArrayList<TaskModel>) :  RecyclerView.Adapter<TaskCustomAdapter.ViewHolder>() {

    private var listener: ItemClickListener? = null
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.tasks_row, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        list[position].let { model -> holder.bindData(model) }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) , View.OnClickListener{

        var taskName : TextView? = null
        var btnEditTask : Button? = null
        var btnRemoveTask : Button? = null

        init {
            taskName = view.findViewById(R.id.taskName)
            btnEditTask = view.findViewById(R.id.editTask)
            btnRemoveTask = view.findViewById(R.id.removeTask)
            btnRemoveTask?.setOnClickListener(this)
            btnEditTask?.setOnClickListener(this)
        }
        override fun onClick(p0: View?) {
                listener?.onItemClick(adapterPosition,p0!!)
        }

        fun bindData(model: TaskModel){
            taskName?.text = model.taskName
        }

    }

    fun setOnItemClickListener(itemClickListener: ItemClickListener) {
        this.listener = itemClickListener
    }

    interface ItemClickListener {
        fun onItemClick(position: Int, view: View)
    }

    fun removeItem(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
    }
}