package com.example.todotaskdemo.utility;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Collection;

public class SharedPrefManager {
    private SharedPreferences sharedpreferences;
    private Context mContext;

    private final String sharedPrefKey = "Demo Todp";
    private SharedPreferences.Editor editor;


    public SharedPrefManager(Context context) {
        mContext = context;
        sharedpreferences = mContext.getSharedPreferences(sharedPrefKey, Context.MODE_PRIVATE);
    }


    //Todo: Remove Prefences when Log off
    public void clearPreferences() {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.apply();
    }


    public void setValue(String key, String value) {
        editor = sharedpreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void setValue(String key, int value) {
        editor = sharedpreferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public void setValue(String key, boolean value) {
        editor = sharedpreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public void setValue(String key, double value) {
        editor = sharedpreferences.edit();
        editor.putFloat(key, (float) value);
        editor.apply();
    }

    public void setValue(String key, long value) {
        editor = sharedpreferences.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public Boolean getBooleanByKey(String key) {
        if (sharedpreferences.contains(key)) {
            return sharedpreferences.getBoolean(key, false);
        }
        return false;
    }

    public String getStringByKey(String key){
        if (sharedpreferences.contains(key)) {
            return sharedpreferences.getString(key, "");
        }
        return null;
    }

    public int getIntegerByKey(String key){
        if (sharedpreferences.contains(key)) {
            return sharedpreferences.getInt(key, 0);
        }
        return 0;
    }

    public float getFloatValue(String name) {
        return sharedpreferences.getFloat(name, 0);
    }

    public long getLongValue(String name) {
        return sharedpreferences.getLong(name, 0);
    }

    public void saveCollectionTOSharedPref(Object collection, String key) {
        String value = new Gson().toJson(collection);
        setValue(key, value);
    }

    public Collection getCollectionFromSharedPref(String key){
        String value = getStringByKey(key);
        GsonBuilder gsonb = new GsonBuilder();
        Gson gson = gsonb.create();
        Collection collection  = gson.fromJson(value, Collection.class);
        return collection;
    }

    public void saveObjectInSharedPref(Object obj, String key) {
        Gson gson = new Gson();
        String json = gson.toJson(obj);
        setValue(key, json);
    }

    public Object getObjectFromSharedPref(String key,Class toCast) {
        Gson gson = new Gson();
        String json = getStringByKey(key);
        if (json!=null&&!json.equals("")){
            return gson.fromJson(json,toCast);
        }else{
            return null;
        }
    }

    public boolean isContains(String key){
        return sharedpreferences.contains(key);
    }

}
