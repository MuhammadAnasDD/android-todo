package com.example.todotaskdemo

import java.io.Serializable

class TaskModel : Serializable {
    var taskName: String? = null
    var taskDiscription: String? = null
    var isSetReminder: Boolean = false
    var reminderDateTime: String? = null

    constructor() {

    }

    constructor(taskName: String, taskDiscription: String, setReminder: Boolean, reminderDateTime: String) {
        this.taskName = taskName
        this.taskDiscription = taskDiscription
        this.isSetReminder = setReminder
        this.reminderDateTime = reminderDateTime
    }
}
