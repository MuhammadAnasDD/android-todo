package com.example.todotaskdemo.helper

class Constants {
    companion object {
        const val TASK_OBJECT = "task_object"
        const val ALL_TASK = "all_task"
        const val TASK_UPDATE = "task_update"
        const val DATE_FORMATE = "MM/dd/yy"
        const val TIME_FORMAT = "hh:mm aa"
    }
}